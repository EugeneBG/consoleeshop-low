﻿using System;
using System.Collections.Generic;

namespace ConsoleEShop
{
    public class ProductDataBase
    {
        private List<Product> db;

        public ProductDataBase()
        {
            db = new List<Product>();
        }

        public ProductDataBase(params Product[] products)
        {
            db = new List<Product>();
            foreach (var item in products)
            {
                db.Add(item);
            }
        }

        public ProductDataBase(IEnumerable<Product> produts)
        {
            db = new List<Product>(produts);
        }

        public void AddNewItem(Product item)
        {
            db.Add(item);
        }

        public bool ProductContains(DataBase db, int id)
        {
            foreach (var item in db.Product.db)
                if (item.Id == id)
                    return true;
            return false;
        }

        public bool ProductContains(DataBase db, string name, string category)
        {
            foreach (var item in db.Product.db)
                if (item.Name.Equals(name) && item.Category.Equals(category))
                    return true;
            return false;
        }

        public void GetInfo()
        {
            foreach (var item in db)
            {
                Console.WriteLine("\n**************************\n");
                Console.WriteLine($"Name : {item.Name}");
                Console.WriteLine($"Price : {item.Price}");
                Console.WriteLine($"Caterogy : {item.Category}");
                Console.WriteLine($"Info : {item.Info}");
                Console.WriteLine("\n**************************\n");
            }
        }

        public Product SearchProduct(DataBase db, string name, string category)
        {

            if (ProductContains(db, name, category))
                return db.Product.db.Find(x => x.Name.Equals(name)
                && x.Category.Equals(category));
            else
                throw new Exception("There is not product with this name and category");
        }
    }
}
