﻿namespace ConsoleEShop
{
    public enum Status
    {
        New,
        Shipped,
        Done, 
        AdminRejected,
        UserRejected,
        Payed,
        Received,
        Completed,
        Invalid
    }
}
