﻿using System;

namespace ConsoleEShop
{
    public class Guest : Visitor
    {
        public void Register(DataBase db, string login, int password, bool as_admin)
        {
            if (!as_admin)
            {
                if (db.User.UserContains(login))
                    throw new Exception("This login is already taken");
                else
                {
                    db.User.AddUser(login, password);
                    Console.WriteLine("\nUser has been added\n");
                }
            }
            else
            {
                if (db.Admin.UserContains(login))
                    throw new Exception("This login is already taken");
                else
                {
                    db.Admin.AddUser(login, password);
                    Console.WriteLine("\nAdmin has been added\n");
                }
            }

        }

        public bool LogIn(DataBase db, string login, int password, bool as_admin)
        {

            if (as_admin)
            {
                if (db.Admin.UserContains(login))
                    return db.Admin.FindUser(login).Password == password;
                else
                    throw new Exception("There is no account");
            }
            else
            {
                if (db.User.UserContains(login))
                    return (db.User.FindUser(login).Password) == password;
                else
                    throw new Exception("There is no account");
            }
        }
    }
}
