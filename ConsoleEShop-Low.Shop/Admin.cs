﻿using System;

namespace ConsoleEShop
{
    public class Admin : Visitor
    {
        public int Password { get; set; }
        public string Login { get; set; }
        private ShoppingCard Card { get; set; } = new ShoppingCard();
        public OrderDataBase OrderHistory { get; set; } = new OrderDataBase();


        public void MakeOrder(Product product)
        {
            if (product is null)
                throw new ArgumentNullException("No products");
            Card.ShopCard.Add(product);
            Console.WriteLine($"\n{product.Name} has been added to shopping card\n");
        }

        public void SetOrdersStatus(DataBase db, int id, Status status, bool user_order)
        {
            if (user_order)
            {
                var a = db.User.db.Find(x => x.OrderHistory.db.Exists(y => y.Id == id));
                var b = a.OrderHistory.db.Find(x => x.Id == id);
                if (!(b is null))
                {
                    switch (status)
                    {
                        case Status.Completed:
                            b.Status = status;
                            break;
                        case Status.Done:
                            b.Status = status;
                            break;
                        case Status.Payed:
                            b.Status = status;
                            break;
                        case Status.Shipped:
                            b.Status = status;
                            break;
                        case Status.AdminRejected:
                            b.Status = status;
                            break;
                        case Status.Received:
                            b.Status = status;
                            break;
                        default:
                            throw new Exception("Invalid status");
                    }
                }
                else
                {
                    throw new Exception("There is not order with this id");
                }

            }
            else
            {
                var a = db.Admin.db.Find(x => x.OrderHistory.db.Exists(y => y.Id == id));
                var b = a.OrderHistory.db.Find(x => x.Id == id);
                if (!(b is null))
                {
                    switch (status)
                    {
                        case Status.Completed:
                            b.Status = status;
                            break;
                        case Status.Done:
                            b.Status = status;
                            break;
                        case Status.Payed:
                            b.Status = status;
                            break;
                        case Status.Shipped:
                            b.Status = status;
                            break;
                        case Status.AdminRejected:
                            b.Status = status;
                            break;
                        case Status.Received:
                            b.Status = status;
                            break;
                        default:
                            throw new Exception("Invalid status");
                    }
                }
                else
                {
                    throw new Exception("There is not order with this id");
                }
            }
        }

        public void View(DataBase db)
        {
            if (db is null)
                throw new NullReferenceException("no reference to database");
            db.Product.GetInfo();
        }

        public void SeeUserInfo(DataBase db, string login)
        {
            if (db.User.UserContains(login))
            {
                var user = db.User.FindUser(login);
                Console.WriteLine("Visitor's info:");
                Console.WriteLine($"UserName : {user.UserName}");
                Console.WriteLine($"Password : {user.Password}");
            }
            else
                throw new Exception("There is not user with this login");
        }

        public void СheckOut(DataBase db)
        {
            if (db is null)
                throw new ArgumentNullException("Database is not connected");

            if (Card.ShopCard.Count == 0)
            {
                Console.WriteLine("\nYour shopping card is empty\n");
            }
            else
            {
                foreach (var item in Card.ShopCard)
                {
                    OrderHistory.AddOrder(db, item);
                }
                Card.ShopCard.Clear();
            }

        }

        public void SeeShoppingCard()
        {

            if (Card.ShopCard.Count == 0)
            {
                Console.WriteLine("\nYour shopping card is emply\n");
            }
            else
            {
                Console.WriteLine("\nYour shopping card:");
                foreach (var item in Card.ShopCard)
                {
                    Console.WriteLine($"\nName of product: {item.Name}\n");
                    Console.WriteLine($"\nPrice of product: {item.Price}\n");
                    Console.WriteLine($"\nInfo about product: {item.Info}\n");
                    Console.WriteLine($"\nCategory of product: {item.Category}\n");
                }
                Console.WriteLine();
            }
        }

        public void ChangeUserInfo(DataBase db, string login, string info)
        {
            var account = db.User.FindUser(login);
            Console.WriteLine("\nInfo has been updated by admin");
        }

        public void AddNewProduct(DataBase db, int price, string name
            , string info, string category)
        {
            db.Product.AddNewItem(new Product()
            {
                Price = price,
                Name = name,
                Info = info,
                Category = category,
            });
        }

        public void ChangeProductInfo(DataBase db, string info, string name, string caterogy)
        {
            if (db.Product.ProductContains(db, name, caterogy))
            {
                var prod = db.Product.SearchProduct(db, name, caterogy);
                prod.Info = info;
            }

            else
                throw new Exception("There is not this product");
        }

    }
}
