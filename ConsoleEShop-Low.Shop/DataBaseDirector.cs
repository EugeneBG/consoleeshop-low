﻿namespace ConsoleEShop
{
    public class DataBaseDirector
    {
        private DataBaseBuilder builder;

        public DataBaseDirector(DataBaseBuilder buildr)
        {
            builder = buildr;
        }

        public void Construct()
        {
            builder.CreateDataBase();
            builder.CreateAdminDB();
            builder.CreateProductDB();
            builder.CreateUserDB();
        }
    }
}
