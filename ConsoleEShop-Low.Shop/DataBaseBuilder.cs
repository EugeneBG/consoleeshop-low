﻿namespace ConsoleEShop
{
    public abstract class DataBaseBuilder
    {
        public abstract void CreateDataBase();
        public abstract void CreateAdminDB();
        public abstract void CreateUserDB();
        public abstract void CreateProductDB();
        public abstract DataBase GetDataBase();
    }
}
