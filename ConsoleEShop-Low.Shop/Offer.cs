﻿using System.Collections.Generic;

namespace ConsoleEShop
{
    public class ShoppingCard
    {
        public List<Product> ShopCard { get; set; }
        public ShoppingCard()
        {
            ShopCard = new List<Product>();
        }
    }
}
