﻿using System;

namespace ConsoleEShop
{
    public abstract class Visitor
    {
        public void View(DataBase db)
        {
            if (db is null)
                throw new NullReferenceException("DataBase is not connected");
            else
                db.Product.GetInfo();
        }

        public Product SearchItem(DataBase db, string name, string caterogy)
        {
            if (db is null)
                throw new NullReferenceException("DataBase is not connected");
            else
                return db.Product.SearchProduct(db, name, caterogy);
        }

    }
}
