﻿using System;
using System.Collections.Generic;

namespace ConsoleEShop
{
    public class OrderDataBase
    {
        public List<Order> db { get; set; }

        public OrderDataBase()
        {
            db = new List<Order>();
        }

        public void AddOrder(DataBase db, params Product[] products)
        {
            if (db is null)
                throw new NullReferenceException("No reference to database");

            if (products is null)
                throw new ArgumentNullException("cant add order");

            foreach (var item in products)
            {
                if (db.Product.ProductContains(db, item.Name, item.Category))
                {
                    this.db.Add(new Order(item));
                    Console.WriteLine($"\nYou made order\n");
                    var a = this.db.Find(x => x.Product == item);
                    Console.WriteLine($"Order's id : {a.Id}");
                }
                else
                    throw new Exception("Product is not in database");
            }

        }


        public OrderDataBase(IEnumerable<Order> orders)
        {
            db = new List<Order>(orders);
        }
    }
}
