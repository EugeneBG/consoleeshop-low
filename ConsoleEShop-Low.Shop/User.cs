﻿using System;

namespace ConsoleEShop
{
    public class User : Visitor
    {
        public int Password { get; set; }
        public string UserName { get; set; }
        private ShoppingCard Card { get; set; } = new ShoppingCard();
        public OrderDataBase OrderHistory { get; set; } = new OrderDataBase();

        public void MakeOrder(Product product)
        {
            if (product is null)
                throw new ArgumentNullException("No products");
            Card.ShopCard.Add(product);
            Console.WriteLine($"\n{product.Name} has been added to shopping card\n");
        }

        public void SetOrdersStatusToReceived(int id)
        {
            if (OrderHistory.db.Count == 0)
            {
                Console.WriteLine("\nYou don't have any order\n");
            }
            else
            {
                var order = OrderHistory.db.Find(x => x.Id == id);
                if (!(order is null))
                {
                    order.Status = Status.Received;
                    Console.WriteLine("Status has been changed");
                }
                else
                    throw new Exception("There is not order in your history of orders");
            }

        }

        public void CheckOut(DataBase db)
        {
            if (db is null)
                throw new ArgumentNullException("Database is not connected");

            if (Card.ShopCard.Count == 0)
            {
                Console.WriteLine("\nYour shopping card is empty\n");
            }
            else
            {
                foreach (var item in Card.ShopCard)
                {
                    OrderHistory.AddOrder(db, item);
                }
                Card.ShopCard.Clear();
            }

        }
        public void SeeShoppingCard()
        {

            if (Card.ShopCard.Count == 0)
            {
                Console.WriteLine("\nYour shopping card is emply\n");
            }
            else
            {
                Console.WriteLine("\nYour shopping card:");
                foreach (var item in Card.ShopCard)
                {
                    Console.WriteLine($"\nName of product: {item.Name}");
                    Console.WriteLine($"Price of product: {item.Price}");
                    Console.WriteLine($"Info about product: {item.Info}");
                    Console.WriteLine($"Category of product: {item.Category}");
                }
                Console.WriteLine("***************");
            }
        }

        public void SeeOrderHistory()
        {
            if (OrderHistory is null)
                throw new NullReferenceException("No reference to database");

            Console.WriteLine("\nHistory of your orders:\n");
            if (OrderHistory.db.Count == 0)
            {
                Console.WriteLine("\nYou don't have any order.\n");
            }
            else
            {
                foreach (var item in OrderHistory.db)
                {
                    Console.WriteLine("\n**************\n");
                    Console.WriteLine($"Status :  {item.Status}");
                    Console.WriteLine($"Id : {item.Id}");
                    Console.WriteLine($"Name :  {item.Product.Name}");
                    Console.WriteLine($"Price :  {item.Product.Price}");
                    Console.WriteLine($"Info :  {item.Product.Info}");
                    Console.WriteLine($"Category :  {item.Product.Category}");
                    Console.WriteLine("\n**************\n\n");
                }
            }
        }

        public void CancelOrder(int id)
        {
            if (OrderHistory.db.Count == 0)
            {
                Console.WriteLine("You don't have any order");
            }
            else
            {
                var order = OrderHistory.db.Find(x => x.Id == id);
                if (!(order is null))
                {
                    if (!OrderHistory.db.Find(x => x.Id == id).Status.Equals(Status.UserRejected))
                    {
                        order.Status = Status.UserRejected;
                    }
                    else
                    {
                        Console.WriteLine("Order's status is already : \"Cancelled by user\"");
                    }
                }
                else
                    throw new Exception("There is not order in your history of orders");
            }

        }

        public void Changeinfo(string name, int password, string login, string info)
        {
            Password = password;
            UserName = login;
            Console.WriteLine("\nUser's data has been changed\n");
        }
    }
}
