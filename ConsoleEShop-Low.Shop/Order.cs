﻿namespace ConsoleEShop
{
    public class Order
    {
        public Status Status { get; set; } = Status.New;
        public Product Product { get; set; }
        static int Count { get; set; } = 0;
        public int Id { get; }
        public Order(Product prod)
        {
            Product = prod;
            Count++;
            Id = Count;
        }
    }
}
