﻿namespace ConsoleEShop
{
    public class ConcreteDataBaseBuilder : DataBaseBuilder
    {
        private DataBase currentBuilder;

        public override void CreateDataBase()
        {
            currentBuilder = new DataBase();
        }

        public override void CreateAdminDB()
        {
            currentBuilder.Admin = make_admin_db();
        }

        public override void CreateProductDB()
        {
            currentBuilder.Product = make_prod_db();
        }

        public override void CreateUserDB()
        {
            currentBuilder.User = make_reg_users_db();
        }

        public override DataBase GetDataBase() => currentBuilder;

        ProductDataBase make_prod_db()
        {
            return new ProductDataBase(
                new Product { Name = "Coke", Price = 20, Category = "Food" },
                new Product { Name = "Playstation", Price = 20000, Category = "Tech" },
                new Product { Name = "First player ready", Price = 150, Category = "Content" });
        }

        UserDb make_reg_users_db()
        {
            return new UserDb(
                new User() { UserName = "AppleJuice", Password = 2345 },
                new User() { UserName = "Silicon Jarry", Password = 3211 });
        }

        AdminDb make_admin_db()
        {
            return new AdminDb(
                new Admin() { Login = "Misterio", Password = 123 },
                new Admin() { Login = "Rey", Password = 321 },
                new Admin() { Login = "Gendo Ikari", Password = 234 });
        }

    }
}
