using System;
using NUnit.Framework;
using ConsoleEShop;

namespace ConsoleEShop_Low.Tests
{
    public class Tests
    {
        [Test]
        public void ThrowExceptionIfAnyUserCanNotViewProductsInNotExistingDataBase()
        {
            //arrange
            var guest = new Guest();
            //assert
            Assert.Throws<NullReferenceException>(() => guest.View(null));
        }

        [Test]
        public void ThrowExceptionDuringCheckoutWhileDataBaseIsNull()
        {
            //arrange
            var user = get_user();
            //assert
            Assert.Throws<ArgumentNullException>(() => user.CheckOut(null));
        }

        [Test]
        public void ThrowExceptionDuringUserRegistrationWhenLoginIsAlreadyTaken()
        {
            //arrange
            var guest = new Guest();
            var as_admin = false;
            var user_db = new UserDb(get_user());
            var db = new DataBase(null, user_db, null);
            var taken_login = get_user().UserName;
            var password = 123;
            //assert
            Assert.Throws<Exception>(() => guest.Register(db, taken_login, password, as_admin));
        }

        [Test]
        public void ThrowExceptionDuringAdminRegistrationWhenLoginIsAlreadyTaken()
        {
            //arrange
            var guest = new Guest();
            var as_admin = true;
            var admin_db = new AdminDb(get_admin());
            var db = new DataBase(null, null, admin_db);
            var taken_login = get_admin().Login;
            var password = 123;
            //assert
            Assert.Throws<Exception>(() => guest.Register(db, taken_login, password, as_admin));
        }

        [Test]
        public void UsersDataBaseThrowsExceptionifCanNotFindUserInDataBase()
        {
            //arrange
            var user_db = new UserDb();
            var incorrect_login = "azerty";
            // act
            Assert.Throws<Exception>(() => user_db.FindUser(incorrect_login));
        }

        [Test]
        public void MenuThrowsExceptionIfDataBaseIsNull()
        {
            Assert.Throws<Exception>(() => new Menu(null).Start());
        }

        [Test]
        public void ExceptionWhileAddingNewOrderWithNotExistingProduct()
        {
            //arrange
            var db = new DataBase();
            var order_db = new OrderDataBase();
            //assert
            Assert.Throws<ArgumentNullException>(() => order_db.AddOrder(db, null));
        }

        [Test]
        public void ExceptionWhileAddingNewOrderToNotExistingDataBase()
        {
            //arrange
            var prod = get_prod();
            var order_db = new OrderDataBase();
            //assert
            Assert.Throws<NullReferenceException>(() => order_db.AddOrder(null, prod));
        }

        [Test]
        public void UserMakeOrderWithNullProductReturnException()
        {
            //arrange
            var user = get_user();
            //assert
            Assert.Throws<ArgumentNullException>(() => user.MakeOrder(null));
        }

        [Test]
        public void AdminMakeOrderWithNullProductReturnException()
        {
            //arrange
            var admin = get_admin();
            //assert
            Assert.Throws<ArgumentNullException>(() => admin.MakeOrder(null));
        }

        [Test]
        public void UserDataBaseNewUserHasBeenAdded()
        {
            //arrane
            var user1 = get_user();
            var user2_login = "user2";
            var user2_password = 123;
            var user_db = new UserDb(user1);
            //act
            user_db.AddUser(user2_login, user2_password);
            var new_admin = user_db.FindUser(user2_login);
            var expected = true;
            var res = new_admin.UserName.Equals(user2_login) && new_admin.Password == user2_password;
            //assert
            Assert.AreEqual(expected, res);
        }

        [Test]
        public void AdminDataBaseNewAdminHasBeenAdded()
        {
            //arrane
            var admin1 = get_admin();
            var admin2_login = "admin2";
            var admin2_password = 123;
            var admin_db = new AdminDb(admin1);
            //act
            admin_db.AddUser(admin2_login, admin2_password);
            var new_admin = admin_db.FindUser(admin2_login);
            var expected = true;
            var res = new_admin.Login.Equals(admin2_login) && new_admin.Password == admin2_password;
            //assert
            Assert.AreEqual(expected, res);
        }

        [Test]
        public void GuestLogInAdminFailed()
        {
            //arrange
            var admin = get_admin();
            var guest = new Guest();
            var admin_db = new AdminDb(admin);
            var db = new DataBase(null, null, admin_db);
            var as_admin = true;
            //act
            var incorrect_password = 0000;
            var res = guest.LogIn(db, admin.Login, incorrect_password, as_admin);
            var expected = false;
            //assert
            Assert.AreEqual(expected, res);
        }

        [Test]
        public void GuestLogInAdminSuccessful()
        {
            //arrange
            var admin = get_admin();
            var guest = new Guest();
            var admin_db = new AdminDb(admin);
            var db = new DataBase(null, null, admin_db);
            var as_admin = true;
            //act
            var res = guest.LogIn(db, admin.Login, admin.Password, as_admin);
            var expected = true;
            //assert
            Assert.AreEqual(expected, res);
        }

        [Test]
        public void GuestLogInUserFailer()
        {
            //arrange
            var user = get_user();
            var guest = new Guest();
            var user_db = new UserDb(user);
            var db = new DataBase(null, user_db, null);
            var as_admin = false;
            //act
            var incorrect_password = 9878;
            var res = guest.LogIn(db, user.UserName, incorrect_password, as_admin);
            var expected = false;
            //assert
            Assert.AreEqual(expected, res);

        }

        [Test]
        public void GuestLogInUserSuccessful()
        {
            //arrange
            var user = get_user();
            var guest = new Guest();
            var user_db = new UserDb(user);
            var db = new DataBase(null, user_db, null);
            var as_admin = false;
            //act
            var res = guest.LogIn(db, user.UserName, user.Password, as_admin);
            var expected = true;
            //assert
            Assert.AreEqual(expected, res);
        }

        [Test]
        public void UserMadeOrder()
        {
            // arrange
            var user = get_user();
            var reg_user_db = new UserDb(user);
            var prod_db = new ProductDataBase(get_prod());
            var db = new DataBase(prod_db, reg_user_db, null);
            // act
            user.MakeOrder(get_prod());
            user.CheckOut(db);
            var res = user.OrderHistory.db.Find(x => x.Product.Equals(get_prod()));
            var expected = Status.New;
            //assert
            Assert.AreEqual(res.Status, expected);
        }

        private User get_user()
        {
            return new User()
            {
                UserName = "qwerty",
                Password = 123,
            };
        }

        private Admin get_admin()
        {
            return new Admin()
            {
                Login = "Qwerty",
                Password = 123,
            };
        }

        private Product get_prod()
        {
            return new Product()
            {
                Name = "PocketBook",
                Price = 120,
                Category = "E-readers",
            };
        }

    }
}