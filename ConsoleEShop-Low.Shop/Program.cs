﻿namespace ConsoleEShop
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DataBaseBuilder builder = new ConcreteDataBaseBuilder();
            DataBaseDirector director = new DataBaseDirector(builder);
            director.Construct();

            new Menu(builder.GetDataBase()).Start();
        }
    }
}
