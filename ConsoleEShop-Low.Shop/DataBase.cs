﻿namespace ConsoleEShop
{
    public class DataBase
    {
        public ProductDataBase Product { get; set; }
        public UserDb User { get; set; }
        public AdminDb Admin { get; set; }

        public DataBase() { }
        public DataBase(ProductDataBase products, UserDb users, AdminDb admins)
        {
            Product = products;
            User = users;
            Admin = admins;
        }

    }
}
